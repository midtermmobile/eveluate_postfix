import 'dart:io';

bool isOperatorSign(String o) {
  return o == '+' ||
    o == '-' ||
    o == '*' ||
    o == '/';
}

bool isValidInfixSyntax(String o){
  return o == '+' ||
    o == '-' ||
    o == '*' ||
    o == '/' ||
    int.tryParse(o) != null;
}
int precedence(String o){
  return (o == '+' || o == '-') ? 1 :
  (o == '*' || o == '/') ? 2 : 0;
}
List infixtopostfix(String infixExpresstion){
  var expression = infixExpresstion;
  var operators =[];
  var postfix = [];

for(int i=0; i < expression.length; i++){
    var token = expression[i];
    if(!isValidInfixSyntax(token))continue;
    if(int.tryParse(token) != null){
      postfix.add(int.tryParse(token));
    }else if(isOperatorSign(token)){
      while(operators.isNotEmpty && operators.last != '(' && precedence(token) < precedence(operators.last)){
        postfix.add(operators.removeLast());
      }
      operators.add(token);
    }else if(token == '('){
      operators.add(token);
    }else if(token == ')'){
      while(operators.last != '('){
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }
  
  while(operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  
  return postfix;
}
void main() {
  var infixExpressionInput = stdin.readLineSync()!;
  var postfix = infixtopostfix(infixExpressionInput);
  print("Infix to Postfix -> $postfix");
}