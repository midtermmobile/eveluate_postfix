import 'dart:io';

num evaluatePosfix(var posfixExpression) {
  var values =[];
  var token = posfixExpression;
  var tokenlist = token.split(' ');
  for (var char in tokenlist) {
    if (double.tryParse(char) != null) {
      values.add(double.tryParse(char));

    }else{
      try{
        num right = values.removeLast();
        num left = values.removeLast();
        num result = 0;
        if (char == '+') {
          result = left+right;
        }else if(char == '-'){
          result = left-right;
        }else if(char == '*'){
          result = left*right;
        }else if(char == '/'){
          result = left/right;
        }
        values.add(result);
      }catch(e){
        print("According to my calculations, the postfix expression $posfixExpression evaluates to NaN.");
        return 0;
      }
    }
  }
    return values[0];

}


void main() {
  var rs = evaluatePosfix(stdin.readLineSync()!);
  print(rs);
}